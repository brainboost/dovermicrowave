# DoverMicrowave

Simple microwave controller. The Purpose of this is to get a sense of your ability to handle requirements, your coding skills and your ability to discuss technical solutions.
Microwave oven controller:
Please supply a complete Visual studio solution including the controller code and unit tests.

Its a very simple microwave oven. It has:

* A heater  Can be turned on or off
* A door  Can be opened and closed by user
* A Start Button  Can be pressed by the user

## User stories:
* When I open door Light is on.
* When I close door Light turns off.
* When I open door heater stops if running.
* When I press start button when door is open nothing happens.
* When I press start button when door is closed, heater runs for 1 minute.
* When I press start button when door is closed and already heating, increase remaining time with 1 minute.
 
Any additional requirements that may be missed can be assumed by you, but please not those down.

PS. As we have no methods in the interface to control the light inside the oven, I added another interface ILightBulb with appropriate methods for controlling the light. 
Of course, it is possible to extend the IMicrowaveOvenHW, either by adding those methods in it or inheriting it from ILightBulb. For the moment it's not a stopover, 
I've added this interface to the constructor's parameters but it wouldn't be problem to refactor it in case of objections. 