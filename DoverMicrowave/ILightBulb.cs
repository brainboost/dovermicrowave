﻿namespace DoverMicrowave
{
    public interface ILightBulb
    {
        bool LightIsOn { get; }

        void TurnOffLight();
        void TurnOnLight();
    }
}