﻿using System;
using System.Timers;

namespace DoverMicrowave
{
    public class MicrowaveOvenController : IDisposable
    {
        public const int Minute = 6 * 1000;     // set it to shorter value for quicker test execution
        private readonly IMicrowaveOvenHW _ovenHardware;
        private readonly ILightBulb _lightBulb;
        private Timer _timer;

        public MicrowaveOvenController(IMicrowaveOvenHW ovenHardware, ILightBulb lightBulb)
        {
            _ovenHardware = ovenHardware;
            _lightBulb = lightBulb;
            _ovenHardware.DoorOpenChanged += OvenHardwareOnDoorOpenChanged;
            _ovenHardware.StartButtonPressed += OvenHardwareOnStartButtonPressed;
        }

        private void OvenHardwareOnStartButtonPressed(object sender, EventArgs eventArgs)
        {
            if (_ovenHardware.DoorOpen) return;

            if (_timer != null)
                _timer.Interval += Minute;
            else
            {
                _timer = new Timer(Minute);
                _ovenHardware.TurnOnHeater();
                _timer.Elapsed += (o, args) =>
                                  {
                                      _ovenHardware.TurnOffHeater();
                                      _timer?.Dispose();
                                      _timer = null;
                                  };
                _timer.Enabled = true;
            }
        }

        private void OvenHardwareOnDoorOpenChanged(bool isOpen)
        {
            if (isOpen == _ovenHardware.DoorOpen) return;

            if (isOpen)
            {
                _lightBulb.TurnOnLight();
                _ovenHardware.TurnOffHeater();
            }
            else
            {
                _lightBulb.TurnOffLight();
            }
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
