﻿namespace DoverMicrowave
{
    public class LightBulb : ILightBulb
    {
        public void TurnOnLight()
        {
            LightIsOn = true;
        }

        public void TurnOffLight()
        {
            LightIsOn = false;
        }

        public bool LightIsOn { get; private set; }
    }
}