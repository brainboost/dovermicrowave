﻿using System;

namespace DoverMicrowave
{
    public class TheHardware : IMicrowaveOvenHW
    {
        public void TurnOnHeater()
        {
            throw new NotImplementedException();
        }

        public void TurnOffHeater()
        {
            throw new NotImplementedException();
        }

        public bool DoorOpen { get; }
        public event Action<bool> DoorOpenChanged;
        public event EventHandler StartButtonPressed;
    }
}