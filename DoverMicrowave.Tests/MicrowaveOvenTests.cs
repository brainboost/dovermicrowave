﻿using System;
using System.Threading;
using NSubstitute;
using NUnit.Framework;

namespace DoverMicrowave.Tests
{
    [TestFixture]
    public class MicrowaveOvenTests
    {
        /*
        User stories:
        When I open door Light is on.
        When I close door Light turns off.
        When I open door heater stops if running.
        When I press start button when door is open nothing happens.
        When I press start button when door is closed, heater runs for 1 minute.
        When I press start button when door is closed and already heating, increase remaining time with 1 minute.
        */
        private MicrowaveOvenController sut;
        private IMicrowaveOvenHW ovenHw;
        private ILightBulb bulb;

        [SetUp]
        public void Init()
        {
            ovenHw = Substitute.For<IMicrowaveOvenHW>();
            bulb = Substitute.For<ILightBulb>();
            sut = new MicrowaveOvenController(ovenHw, bulb);
        }
        [Test]
        public void When_I_open_door_Light_is_on()
        {
            // nsubstitute raises the door open event
            ovenHw.DoorOpenChanged += Raise.Event<Action<bool>>(true);
            bulb.Received(1).TurnOnLight();
        }
        [Test]
        public void When_I_close_door_Light_is_off()
        {
            ovenHw.DoorOpen.Returns(true);
            ovenHw.DoorOpenChanged += Raise.Event<Action<bool>>(false);
            bulb.Received(1).TurnOffLight();
        }

        [Test]
        public void When_I_open_door_heater_stops_if_running()
        {
            ovenHw.DoorOpen.Returns(false);
            ovenHw.DoorOpenChanged += Raise.Event<Action<bool>>(true);
            ovenHw.Received(1).TurnOffHeater();
        }

        [Test]
        public void When_I_press_start_button_when_door_is_open_nothing_happens()
        {
            ovenHw.DoorOpen.Returns(true);
            ovenHw.StartButtonPressed += Raise.Event();

            ovenHw.DidNotReceive().TurnOnHeater();
            ovenHw.DidNotReceive().TurnOffHeater();
        }

        [Test]
        public void When_I_press_start_button_when_door_is_closed_heater_runs_for_1_minute()
        {
            ovenHw.DoorOpen.Returns(false);
            ovenHw.StartButtonPressed += Raise.Event();

            Received.InOrder(() =>
                             {
                                 ovenHw.TurnOnHeater();
                                 Thread.Sleep(MicrowaveOvenController.Minute + 1);
                                 ovenHw.TurnOffHeater();
                             });
        }

        [Test]
        public void
            When_I_press_start_button_when_door_is_closed_and_already_heating_increase_remaining_time_with_1_minute()
        {
            ovenHw.DoorOpen.Returns(false);
            ovenHw.StartButtonPressed += Raise.Event();
            ovenHw.StartButtonPressed += Raise.Event();

            Received.InOrder(() =>
                             {
                                 ovenHw.TurnOnHeater();
                                 Thread.Sleep(MicrowaveOvenController.Minute * 2 + 1);
                                 ovenHw.TurnOffHeater();
                             });
        }
    }
}
